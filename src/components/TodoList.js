import React, { Component } from 'react';
import '../App.css'
import FlipMove from 'react-flip-move';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      act: 0,
      index: 0,
      items: []
    };
  }

  //Add an Item
  addItems = (e) => {
    e.preventDefault()

    let items = this.state.items
    let task = this.refs.task.value
    let title = this.refs.title.value
    let date = new Date()
    let timeString = date.getHours() + ":" + date.getMinutes()
    let dateString = date.getDate() + "/" + date.getMonth() + "/" + date.getFullYear()

    let dates = {
      timeString, dateString
    }

    //if act doesn't equal 0 edit task without adding a new task
    if (this.state.act === 0) {
      let item = {
        task, title, dates
      }

      items.push(item)

    } else {
      let index = this.state.index
      items[index].task = task
      items[index].title = title
    }


    this.setState({
      items: items
    })

    this.refs.myForm.reset()
    this.refs.title.focus()
  }

// Remove an Item
removeItem = (i) => {
  let items = this.state.items
  items.splice(i,1)
  this.setState({
    items: items
  })
}

//Edit an Item
editItem = (i) => {
  let item = this.state.items[i]
  this.refs.task.value = item.task
  this.refs.title.value = item.title

  this.setState({
    act: 1,
    index: i
  })

  this.refs.task.focus()
}

  render() {
    return (
      <div className="todoListMain container mt-5">
        <div className="header row">
          <div className="col-md-12">
            <form onSubmit={(e) => {this.addItems(e)}} ref="myForm" className="myForm">
              <label>Title :</label>
              <input type="text" ref="title" className="formField" placeholder="Your memo title" />
              <label>Note  :</label>
              <input type="text" ref="task" className="formField" placeholder="Make a memo"/>
              <button type="button" className="myButton shadow" onClick={(e) => this.addItems(e)}>Add</button>
            </form>
          </div>
        </div>


        <div className="row">
          <div className="col-md-12">
            <FlipMove duration={250} easing="ease-out">
              {this.state.items.map((item,i) =>
                <div className="noteField container shadow">
                  <div className="row">
                    <div className="col-md-4 text-left">
                      <h4>{i+1}.</h4>
                    </div>
                    <div className="col-md-4">
                      <h4>{item.title}</h4> <br/> <pre>{item.dates.timeString} {item.dates.dateString}</pre> <hr/>
                    </div>
                    <div className="col-md-4 text-right">
                      <a className="edit" onClick={() => {this.editItem(i)}}><i className="fas fa-edit"></i></a>
                        <a className="delete" onClick={() => {this.removeItem(i)}}>X</a>
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-md-12 ">
                      <p className="textField">{item.task}</p>
                    </div>
                  </div>

                </div>
              )}
            </FlipMove>
          </div>
        </div>
      </div>
    );
  }

}

export default TodoList;
