import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import TodoList from './components/TodoList';

class App extends Component {
  render() {
    return (
      <div className="App container">
        <div className="row">
          <div className="col-md-12">
            <header className="App-header">
              <img src={logo} className="App-logo" alt="logo" />
              <h1 className="App-title">MEMO LIST APP</h1>
            </header>
          </div>
        </div>
        <TodoList />
      </div>
    );
  }
}

export default App;
